<?php
function tentukan_nilai ($int) {
    if($int >= 85 && $int <= 100) {
        return "Sangat Baik";
    } else if ($int >= 70 && $int < 85) {
        return "Baik";
    } else if ($int >= 60 && $int < 70) {
        return "Cukup";
    } else {
        return "Kurang";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang