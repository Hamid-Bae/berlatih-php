<?php

function tukar_besar_kecil($string){
    $alphabet = "abcdefghijklmnopqrstuvwxyz";
    $alphabetCap = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $newString = "";

    $arrString = str_split($string);
    $arrAlphabet = str_split($alphabet);
    $arrAlphabetCap = str_split($alphabetCap);

    for ($i = 0; $i < count($arrString); $i++) {
        if (in_array($arrString[$i], $arrAlphabet)) {
            $indexString = array_search($arrString[$i], $arrAlphabet);
            $newString .= $arrAlphabetCap[$indexString];
        } else if (in_array($arrString[$i], $arrAlphabetCap)) {
            $indexString = array_search($arrString[$i], $arrAlphabetCap);
            $newString .= $arrAlphabet[$indexString];
        } else {
            $newString .= $string[$i];
        }
    }

    return $newString . "\n";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>